@extends('layouts.frontend')

@section('header')
<title>Home | PRPM Walen - Pimpinan Ranting Pemuda Muhammadiyah</title>
@stop

@section('content')
@if(session('error'))
    <div class="alert alert-danger" role="alert">
        {{session('error')}}
    </div>
@endif
<div id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="fill" style="background-image:url('{{asset('/frontend')}}/img/banner-slide-1.jpg');"></div>
                <div class="carousel-caption slide-up">
                    <h1 class="banner_heading">Stand on The <span>Social </span>Humanity</h1>
                    <p class="banner_txt">Donor darah, Santunan (korban bencana, dhuafa, yatim dan piatu), Pengobatan gratis dan Kegiatan Sosial kemasyarakatan lainnya</p>
                </div>
            </div>

            <div class="item">
                <div class="fill" style="background-image:url('{{asset('/frontend')}}/img/banner-slide-2.jpg');"></div>
                <div class="carousel-caption slide-up">
                    <h1 class="banner_heading"><span>Donor Darah </span>Bersama PMI Boyolali</h1>
                    <p class="banner_txt">Acara diselenggarakan di MIM 1 Walen. Diikuti oleh 56 peserta dari desa Walen dan sekitarnya, bahkan beberapa peserta dari Kab. Semarang. </p>
                </div>
            </div>

            <div class="item">
                <div class="fill" style="background-image:url('{{asset('/frontend')}}/img/banner-slide-3.jpg');"></div>
                <div class="carousel-caption slide-up">
                    <h1 class="banner_heading"><span>Santunan </span>Dhuafa</h1>
                    <p class="banner_txt">Santunan diberikan kepada warga dukuh Ngampon yang ekonominya masih kategori bawah</p>
                </div>
            </div>
        </div>

        <!-- Left and right controls -->

        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> 
            <i class="fa fa-angle-left" aria-hidden="true"></i>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> 
            <i class="fa fa-angle-right" aria-hidden="true"></i>
            <span class="sr-only">Next</span>
        </a>

    </div>

    <section id="features">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-12 block">
                    <div class="col-md-2 col-xs-2"><i class="fa fa-support feature_icon"></i></div>
                    <div class="col-md-10 col-xs-10">
                        <h4>Kegiatan Menarik</h4>
                        <p>PRPM Walen memiliki kegiatan donor darah rutin tiga bulan yang bekerja sama dengan PMI Boyolali, Santunan korban bencana, yatim dan dhuafa, Pengobatan gratis dan Kegiatan sosial kemasyarakatan lainnya</p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 block">
                    <div class="col-md-2 col-xs-2"><i class="fa fa-bullhorn feature_icon"></i></div>
                    <div class="col-md-10 col-xs-10">
                        <h4>Berjiwa Muda</h4>
                        <p>PRPM Walen terdiri dari anak muda yang memiliki ide-ide baru, semangat, action, langkah, dedikasi dan jiwa kemanusiaan tinggi</p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 block">
                    <div class="col-md-2 col-xs-2"><i class="fa fa-laptop feature_icon"></i></div>
                    <div class="col-md-10 col-xs-10">
                        <h4>Edukasi Masyarakat</h4>
                        <p>Tidak hanya membantu membantu masyarakat dalam permasalahan duniawi, PRPM Walen juga memberikan pengajian akbar sebagai wujud edukasi keagamaan kepada masyarakat supaya hidup menjadi terarah dengan tuntunan Al Quran dan Sunnah</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


@stop
