@extends('layouts.frontend')

@section('header')
<title>Login | PRPM Walen - Pimpinan Ranting Pemuda Muhammadiyah</title>
@stop

@section('content')
<section id="top_banner">
    <div class="banner">
        <div class="inner text-center">
            <h2>PRPM | Pimpinan Ranting Pemuda Muhammadiyah Walen</h2>
        </div>
    </div>
    <div class="page_info">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8 col-xs-6">
                    <h4>Tentang</h4>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<section id="about-page-section-3">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-7 text-align">
                <div class="section-heading">
                    <h2>Tentang <span>PRPM Walen</span></h2>
                    <p class="subheading">Fastabiqul Khairat</p>
                </div>
                <p align="justify">
                    Pemuda Muhammadiyah merupakan organisasi otonom di lingkungan Muhammadiyah yang merupakan gerakan dakwah Islam amar ma'ruf nahi mungkar di kalangan pemuda yang beraqidah Islam dan bersumber pada al-Quran dan Sunnah Rasul. Pemuda Muhammadiyah didirikan dengan tujuan untuk menghimpun, membina dan menggerakkan potensi Pemuda Islam serta meningkatkan perannya sebagai kader untuk mencapai tujuan Muhammadiyah.
                </p>
                <p align="justify">
                    PRPM Walen merupakan organisasi Pemuda Muhammadiyah di tingkat desa Walen. PRPM sendiri kepanjangan dari Pimpinan Ranting Pemuda Muhammadiyah. PRPM Walen berkontribusi disekitar Desa Walen yang meliputi Dukuh Walen, Dukuh Jeringan, Dukuh Ngampon, Dukuh Wates, Dukuh Jaten, Dukuh Manglen, Dukuh Sambiroto, Dukuh Pacingan dan Dukuh Pokoh. PRPM Walen memiliki kegiatan santunan dhuafa, korban bencana, yatim dan piatu, pengobatan massal gratis, donor darah dan kegiatan sosial kemasyarakatan lainnya.
                </p>
                <p align="justify">
                    <b>VISI</b>
                    <br> Mempersiapkan kader dan generasi muda Indonesia untuk siap menghadapi tantangan masa depan yang lebih beragam, penuh dinamika dan berbagai kepentingan dalam rangka mencapai maksud dan tujuan Pemuda Muhammadiyah.
                </p>
                <p align="justify">
                    <b>MISI</b>
                    <br>Menjadikan gerakan dakwah amar ma'ruf nahi mungkar, gerakan keilmuan, gerakan sosial kemasyarakatan dan gerakan kewirausahaan sebagai tumpuan kegiatan dengan memahami setiap persoalan yang timbul dan kebutuhan lingkungan dimana Pemuda Muhammadiyah melakukan amal karya nyatanya.
                </p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-5">
                <img height="" width="auto" src="{{asset('/frontend')}}/img/iphone62.png" class="attachment-full img-responsive" alt="">
            </div>
        </div>
    </div>
</section>
<section id="skills">
    <div class="titlebar">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 xol-md-12 col-sm-12 col-xs-12">
                    <div class="section-heading text-center">
                        <h2>Departemen <span>Organisasi</span></h2>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-6 block mybox">
                            <div class="progress color progress-bar-4">
                                <span class="progress-left">
                                    <span class="progress-bar"></span>
                                </span>
                                <span class="progress-right">
                                    <span class="progress-bar"></span>
                                </span>
                                <div class="progress-value">85%</div>

                            </div>
                            <div class="progress-title">
                                <h5>Pendidikan</h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 block mybox">
                            <div class="progress color progress-bar-1">
                                <span class="progress-left">
                                    <span class="progress-bar"></span>
                                </span>
                                <span class="progress-right">
                                    <span class="progress-bar"></span>
                                </span>
                                <div class="progress-value">95%</div>
                            </div>
                            <div class="progress-title">
                                <h5>Sosial</h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 block mybox">
                            <div class="progress color progress-bar-3">
                                <span class="progress-left">
                                    <span class="progress-bar"></span>
                                </span>
                                <span class="progress-right">
                                    <span class="progress-bar"></span>
                                </span>
                                <div class="progress-value">70%</div>
                            </div>
                            <div class="progress-title">
                                <h5>Olahraga</h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 block mybox">
                            <div class="progress color progress-bar-2">
                                <span class="progress-left">
                                    <span class="progress-bar"></span>
                                </span>
                                <span class="progress-right">
                                    <span class="progress-bar"></span>
                                </span>
                                <div class="progress-value">80%</div>
                            </div>
                            <div class="progress-title">
                                <h5>Kewirausahaan</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</section>
<section id="team-member">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 xol-md-12 col-sm-12 col-xs-12">
                <div class="section-heading text-center">
                    <h1>PRPM Walen <span>Squad</span></h1>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/alik.jpg" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle">
                                                <i class="qodef-icon-simple-line-icon qodef-icon-element fa fa-cog"></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">Ketua</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holderre">
                                            <h5 class="our-team-name" style="text-transform: none">M. Ali Mustofa</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/arif.jpg" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle">
                                                <i class="qodef-icon-simple-line-icon fa fa-pencil qodef-icon-element"></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">Sekretaris</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">Arif Suryawan</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/wahid.jpg" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle">        
                                                    <i class="qodef-icon-simple-line-icon fa fa-hashtag qodef-icon-element"></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">Bendahara</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">Wahid Maryanto</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/rochim.jpg" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">Pendidikan</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">M. Rochim</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/koris.jpg" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">Sosial</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">M. Mukoris</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/rozaq.jpg" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">Kewirausahaan</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">M. Abdurrozaq</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/arifs.jpg" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">Sosial</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">Arif Santoso</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/kiki.jpg" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">Olahraga</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">Rifki Adi S</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/irwan.jpg" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">Kewirausahaan</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">Irwan Susilo</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/iqbal.jpg" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">Kesehatan</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">M. Iqbal</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/huda.jpg" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">APA</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">M. Miftahul Huda</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/yunto.jpg" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">APA</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">Wahyunto</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/adi.jpg" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">Kewirausahaan</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">Fauzi Adi N</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/hafid.jpg" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">APA</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">Abdul Hafid</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/adam.jpg" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">APA</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">Adam</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/hanif.jpg" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">Kesehatan</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">Hanif Mustofa</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/avatarDefault.png" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">APA</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">Joko</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/avatarDefault.png" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">APA</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">Tri Riyadi</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/avatarDefault.png" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">APA</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">Narto</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/gunawan.jpg" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">Kewirausahaan</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">A. N. Gunawan</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/toriq.jpg" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">APA</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">M Thariq Aziz</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/avatarDefault.png" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">Pendidikan</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">Wahyu Kuncorojati</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container col-md-3 col-sm-6 col-xs-6 block mybox">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="our-team main-info-below-image">
                                <div class="our-team-inner">
                                    <div class="our-team-image">
                                        <img src="{{asset('/frontend')}}/img/avatarDefault.png" />
                                        <div class="qodef-circle-animate"></div>
                                        <div class="our-team-position-icon">
                                            <span class="qodef-icon-shortcode circle ">
                                                    <i class="qodef-icon-simple-line-icon fa fa-wrench qodef-icon-element" ></i>
                                            </span>
                                        </div>
                                        <h6 class="q_team_position">Kesehatan</h6>
                                    </div>
                                    <div class="our-team-info">
                                        <div class="our-team-title-holder">
                                            <h5 class="our-team-name" style="text-transform: none">Habib Maksum A</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                


            </div>
        </div>
    </div>
</section>
@stop 
