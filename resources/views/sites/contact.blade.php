@extends('layouts.frontend')

@section('header')
<title>Kontak | PRPM Walen - Pimpinan Ranting Pemuda Muhammadiyah</title>
@stop

@section('content')
<section id="top_banner">
    <div class="banner">
        <div class="inner text-center">
            <h2>PRPM | Pimpinan Ranting Pemuda Muhammadiyah Walen</h2>
        </div>
    </div>
    <div class="page_info">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8 col-xs-6">
                    <h4>Kontak</h4>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

    <section id="contact">
        <div class="overlay">
            <div class="gmap-area">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="gmap">
                                <iframe src="https://www.google.com/maps/embed?pb=!4v1582016104019!6m8!1m7!1sIyoQOQrr_nvQl3P6Gx8Vmw!2m2!1d-7.442050113077422!2d110.64642632489!3f10.41332069035843!4f-4.353149563353583!5f1.7597337603299854" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                            </div>
                        </div>
                        <div class="col-sm-7 map-content">
                            <ul class="row">
                                <li class="col-sm-6">
                                    <address>
									<h5>Sekretariat</h5>
									<p>PRPM Walen <br>
									Jl. Singoprono Raya KM. 04,<br>
                                    Walen, Kec. Simo, Kab. Boyolali<br>
                                    (Depan MI Muhammadiyah 1 Walen)<br>
									</p>
                                        <p>Telp: 0878 3642 0202<br>
                                        No. Rek BRI: 6658-01-027887-53-3<br>
                                        a.n. PRPM Walen</p>
								</address>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop