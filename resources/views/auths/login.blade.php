@extends('layouts.frontend')

@section('header')
<title>Login | PRPM Walen - Pimpinan Ranting Pemuda Muhammadiyah</title>
@stop

@section('content')
<section id="top_banner">
	<div class="banner">
		<div class="inner text-center">
			<h2>Donor Darah PRPM Walen</h2>
		</div>
	</div>
</section>
<section id="login-reg">
	<div class="container">
		<!-- Top content -->
		<div class="row">
			@if(session('success'))
                <div class="alert alert-success" role="alert">
                    {{session('success')}} 
                </div>
            @endif
            @if(session('error'))
                <div class="alert alert-danger" role="alert">
                    {{session('error')}}
                </div>
			@endif
			<div class="col-md-6 col-sm-12 forms-right-icons">
				<div class="section-heading">
					<h2>Login untuk masuk ke <span>Aplikasi</span></h2>
					<p class="subheading"></p>
				</div>
				<div class="row">
					<div class="col-xs-2 icon"><i class="fa fa-support"></i></div>
					<div class="col-xs-10 datablock">
						<h4>Kegiatan menarik</h4>
						<p>PRPM Walen memiliki kegiatan donor darah rutin tiga bulan yang bekerja sama dengan PMI Boyolali, Santunan korban bencana, yatim dan dhuafa, Pengobatan gratis dan Kegiatan sosial kemasyarakatan lainnya</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-2 icon"><i class="fa fa-bullhorn"></i></div>
					<div class="col-xs-10 datablock">
						<h4>Banyak teman</h4>
						<p>Organisasi kami terdiri dari anak muda yang memiliki semangat dan jiwa kemanusiaan tinggi</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-12">
				<div class="form-box">
					<div class="form-top">
						<div class="form-top-left">
							<h3>Login</h3>
							<p>Masukkan email dan password untuk login:</p>
						</div>
						<div class="form-top-right">
							<i class="fa fa-key"></i>
						</div>
					</div>
					<div class="form-bottom">
						<form role="form" class="login-form" action="/userlogin" method="POST">
						{{csrf_field()}}
							<div class="input-group form-group {{$errors->has('email') ? ' has-error' : ''}}">
								<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
								<input name="email" type="email" class="form-control" placeholder="Email..." value="guest1@donor.com" aria-describedby="basic-addon1">
							</div>
							<div class="input-group form-group {{$errors->has('password') ? ' has-error' : ''}}">
								<span class="input-group-addon" id="basic-addon1"><i class="fa fa-unlock"></i></span>
								<input name="password" type="password" class="form-control" placeholder="Password..." value="12345" aria-describedby="basic-addon1">
							</div>
							<button type="submit" class="btn">Login</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop
