@extends('layouts.master')
@section('header')
<title>Dashboard | PRPM Walen - Pimpinan Ranting Pemuda Muhammadiyah</title>
@stop

@section('content')
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			@if(session('success'))
                <div class="alert alert-success" role="alert">
                    {{session('success')}} 
                </div>
			@endif
            @if(session('errors'))
                <div class="alert alert-danger" role="alert">
					{{session('errors')}}
                </div>
            @endif
			<!-- STATISTIK -->
			<div class="panel panel-headline">
				<div class="panel-heading">
					<h3 class="panel-title">Statistik Donor Darah</h3>
					<p class="panel-subtitle">Periode: 01-05-2016 -- {{date('d-m-Y')}}</p>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-4">
							<div class="metric">
								<span class="icon"><i class="lnr lnr-user"></i></span>
								<p>
									<span class="number">
										<a href="/donor">{{donorTotal()}}</a>
									</span>
									<span class="title">Total Pendonor Darah</span>
								</p>
							</div>
						</div>
						<div class="col-md-4">
							<div class="metric">
								<span class="icon"><i class="lnr lnr-layers"></i></span>
								<p>
									<span class="number">
										<a href="/donorevent">{{donorEventTotal()}}</a>
									</span>
									<span class="title">Total Kegiatan Donor Darah</span>
								</p>
							</div>
						</div>
						<div class="col-md-4">
							<div class="metric">
								<span class="icon"><i class="lnr lnr-calendar-full"></i></span>
								<p>
									<span class="number">
										<a href="/donorevent">{{date('d-m-Y', strtotime(donorEventLast()))}}</a>
									</span>
									<span class="title">Tanggal Donor Darah Terakhir</span>
								</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="metric">
								<span class="icon"><i class="lnr lnr-drop"></i></span>
								<p>
									<span class="number">
										<a href="/dashboard/a">{{bloodA()}}</a>
									</span>
									<span class="title">Goldar A</span>
								</p>
							</div>
						</div>
						<div class="col-md-3">
							<div class="metric">
								<span class="icon"><i class="lnr lnr-drop"></i></span>
								<p>
									<span class="number">
										<a href="/dashboard/b">{{bloodB()}}</a>
									</span>
									<span class="title">Goldar B</span>
								</p>
							</div>
						</div>
						<div class="col-md-3">
							<div class="metric">
								<span class="icon"><i class="lnr lnr-drop"></i></span>
								<p>
									<span class="number">
										<a href="/dashboard/ab">{{bloodAB()}}</a>
									</span>
									<span class="title">Goldar AB</span>
								</p>
							</div>
						</div>
						<div class="col-md-3">
							<div class="metric">
								<span class="icon"><i class="lnr lnr-drop"></i></span>
								<p>
									<span class="number">
										<a href="/dashboard/o">{{bloodO()}}</a>
									</span>
									<span class="title">Goldar O</span>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END STATISTIK -->
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
						<div id="participant_event_chart"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-7">
					<!-- THE MOST DONORS -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title"><strong>Pendonor Darah Terbanyak</strong></h3>
							<div class="right">
								<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
								<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
							</div>
						</div>
						<div class="panel-body no-padding">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama</th>
										<th>Alamat</th>
										<th>Goldar</th>
										<th>Total Donor</th>
									</tr>
								</thead>
								<tbody>
									@php
										$rankk = 1;
									@endphp
									@foreach(ranks() as $rank)
									<tr>
										<td>{{$rankk}}</td>
										<td>{{$rank->fullname}}</td>
										<td>{{explode(' ', trim($rank->address))[0]}}</td>
										<td>{{strtoupper($rank->blood_group)}}</td>
										<td>{{$rank->donorEventTotalPassed()}}</td>
									</tr>
									@php
										$rankk ++;
									@endphp
									@endforeach
								</tbody>
							</table>
						</div>
						<div class="panel-footer">
							<div class="row">
								<div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i>  24 jam terakhir</span></div>
								<div class="col-md-6 text-right"><a href="#" class="btn btn-primary">Tampilkan semua</a></div>
							</div>
						</div>
					</div>
					<!-- END MOST DONORS -->
				</div>
				<div class="col-md-5">
					<div class="panel">
						<div id="blood_group_chart"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<footer>
	<div class="container-fluid">
		<p class="copyright">Shared by <i class="fa fa-love"></i><a target="_blank" href="https://bootstrapthemes.co">BootstrapThemes</a></p>
	</div>
</footer>
@stop

@section('footer')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script>
	Highcharts.chart('blood_group_chart', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title: {
			text: 'Golongan Darah'
		},
		subtitle: {
			text: 'Realtime chart'
		},
		tooltip: {
			pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.percentage:.1f} %'
				},
				showInLegend: true
			}
		},
		series: [{
			name: 'Goldar',
			colorByPoint: true,
			data: [{
				name: 'A',
				y: {{bloodA()}},
				sliced: false,
				selected: false
			}, {
				name: 'Belum tau',
				y: {{bloodBelum()}}
			}, {
				name: 'B',
				y: {{bloodB()}}
			}, {
				name: 'AB',
				y: {{bloodAB()}}
			}, {
				name: 'O',
				y: {{bloodO()}}
			}]
		}]
	});
</script>
<script>
	Highcharts.chart('participant_event_chart', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'Peserta Kegiatan Donor Darah'
		},
		subtitle: {
			text: 'Realtime chart'
		},
		xAxis: {
			categories: {!!json_encode($date)!!},
			crosshair: true
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Jumlah (orang)'
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'  +
				'<td style="padding:0"><b>{point.y:.1f} orang</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		series: [{
			name: 'Total Peserta',
			data: {!!json_encode($participant)!!}
		},{
			name: 'Lolos',
			data: {!!json_encode($passed)!!}
		},{
			name: 'Tidak lolos',
			data: {!!json_encode($reject)!!}
		}]
	});
</script>
@stop
