@extends('layouts.master')
@section('header')
<title>Goldar AB | PRPM Walen - Pimpinan Ranting Pemuda Muhammadiyah</title>
@stop

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            @if(session('success'))
                <div class="alert alert-success" role="alert">
                    {{session('success')}} 
                </div>
			@endif
            @if(session('errors'))
                <div class="alert alert-danger" role="alert">
                    {{session('errors')}}
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Daftar Pendonor Golongan Darah AB</strong></h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Tanggal Lahir</th>
                                        <th>No. HP</th>
                                        <th>Profesi</th>
                                        <th>Alamat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no_urut = 1;
                                    @endphp
                                    @foreach($goldarAB as $ab)
                                    <tr>
                                        <td>{{$no_urut}}</td>
                                        <td>
                                            <a href="/donor/{{$ab->id}}/profile">
                                            {{$ab->fullname}}
                                            </a>
                                        </td>
                                        <td>{{date('d-m-Y', strtotime($ab->birth_date))}}</td>
                                        <td>{{$ab->phone}}</td>
                                        <td>{{$ab->religion}}</td>
                                        <td>{{$ab->address}}</td>
                                    </tr>
                                    @php
                                        $no_urut ++;
                                    @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
