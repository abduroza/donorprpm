@extends('layouts.master')
@section('header')
<title>Goldar A | PRPM Walen - Pimpinan Ranting Pemuda Muhammadiyah</title>
@stop

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            @if(session('success'))
                <div class="alert alert-success" role="alert">
                    {{session('success')}} 
                </div>
			@endif
            @if(session('errors'))
                <div class="alert alert-danger" role="alert">
                    {{session('errors')}}
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Daftar Pendonor Golongan Darah A</strong></h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Tanggal Lahir</th>
                                        <th>No. HP</th>
                                        <th>Profesi</th>
                                        <th>Alamat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no_urut = 1;
                                    @endphp
                                    @foreach($goldarA as $a)
                                    <tr>
                                        <td>{{$no_urut}}</td>
                                        <td>
                                            <a href="/donor/{{$a->id}}/profile">
                                            {{$a->fullname}}
                                            </a>
                                        </td>
                                        <td>{{date('d-m-Y', strtotime($a->birth_date))}}</td>
                                        <td>{{$a->phone}}</td>
                                        <td>{{$a->religion}}</td>
                                        <td>{{$a->address}}</td>
                                    </tr>
                                    @php
                                        $no_urut ++;
                                    @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
