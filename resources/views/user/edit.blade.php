@extends('layouts.master')
@section('header')
<title>Edit User | PRPM Walen - Pimpinan Ranting Pemuda Muhammadiyah</title>
@stop

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            @if(session('success'))
                <div class="alert alert-success" role="alert">
                    {{session('success')}} 
                </div>
			@endif
            @if(session('errors'))
                <div class="alert alert-danger" role="alert">
                    {{session('errors')}}
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Edit User</strong></h3>
                        </div>
                        <div class="panel-body">
                            <form action="/user/{{$data_user->id}}/useredit" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="form-group col-md-4 {{$errors->has('fullname') ? ' has-error' : ''}}">
                                        <label for="fullname">Nama Lengkap</label>
                                        <input name="fullname" type="text" class="form-control" id="fullname" aria-describedby="emailHelp" placeholder="Nama lengkap..." value="{{$data_user->fullname}}">
                                        @if($errors->has('fullname'))
                                            <span class="help-block">{{$errors->first('fullname')}}</span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-3 {{$errors->has('nik') ? ' has-error' : ''}}">
                                        <label for="email">Email</label>
                                        <input name="email" type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="email..." value="{{$data_user->email}}">
                                        @if($errors->has('email'))
                                            <span class="help-block">{{$errors->first('email')}}</span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-3 {{$errors->has('password') ? ' has-error' : ''}}">
                                        <label for="exampleInputEmail1">Password</label>
                                        <input name="password" type="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Password..." >
                                        @if($errors->has('password'))
                                            <span class="help-block">{{$errors->first('password')}}</span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="roleUser">Peran</label>
                                        <select name="role" class="form-control" id="roleUser">
                                            <option value="admin" @if($data_user->role == "admin") selected @endif>Admin</option>
                                            <option value="donor" @if($data_user->role == "donor") selected @endif>Donor</option>
                                            <option value="guest" @if($data_user->role == "guest") selected @endif>Guest</option>
                                        </select>
                                    </div>
                                </div>
                                  <br>
                                <button type="submit" class="btn btn-warning">Update</button>
                                <a href="javascript:history.back()" class="btn btn-default" >Batal</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
