@extends('layouts.master')

@section('header')
<title>Profil Pendonor | PRPM Walen - Pimpinan Ranting Pemuda Muhammadiyah</title>
<!-- this link used by vitalet editable -->
<link href="{{asset('admin/assets/vitaletsEditable/css/bootstrap-editable.css')}}" rel="stylesheet"/>
@stop

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
        @if(session('success'))
            <div class="alert alert-success" role="alert">
                {{session('success')}} 
                <!-- session is fungction(), while 'sukses' is variable that declared in the DonorController-->
            </div>
        @endif
        @if(session('errors'))
            <div class="alert alert-danger" role="alert">
                {{session('errors')}}
            </div>
        @endif
            <div class="panel panel-profile">
                <!-- <div class="clearfix"> -->
                    <div class="profile-left">
                        <div class="profile-header">
                            <div class="overlay"></div>
                            <div class="profile-main">
                                <img src="{{getAvatar($data_pendonor->id)}}" class="img-circle" alt="Avatar">
                                <!-- getAvatar() is function that made in Donor model -->
                                <h3 class="name">{{$data_pendonor->fullname}}</h3>
                                <span class="online-status status-available">Available</span>
                            </div>
                            <div class="profile-stat">
                                <div class="row">
                                    <div class="col-md-4 stat-item">{{$data_pendonor->donorEventTotalPassed()}}<span>Kali donor</span>
                                    </div>
                                    <div class="col-md-4 stat-item">{{($data_pendonor->donorEventPassedLast())}}<span>Donor terakhir</span>
                                    </div>
                                    <div class="col-md-4 stat-item">
                                    {{$data_pendonor->donorEventTotalPassed() * 45}} <span>Points</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="profile-detail">
                            <div class="profile-info">
                                <h4 class="heading">Identitas Pendonor</h4>
                                <ul class="list-unstyled list-justify">
                                    <li>Nama<span>{{$data_pendonor->fullname}}</span></li>
                                    <li>Golongan darah<span>{{strtoupper($data_pendonor->blood_group)}}</span></li>
                                    <li>NIK<span>{{$data_pendonor->nik}}</span></li>
                                    <li>Jenis Kelamin<span>{{strtoupper($data_pendonor->gender)}}</span></li>
                                    <li>Tempat lahir<span>{{$data_pendonor->birth_place}}</span></li>
                                    <li>Tanggal lahir <span>{{date('l, d-m-Y', strtotime($data_pendonor->birth_date))}}</span></li>
                                    <li>Umur<span>{{getAge($data_pendonor->id)}} tahun</span></li>
                                        <!-- getAge() is function that made in app/Helpers/Global.php -->
                                    <li>No. HP<span>{{$data_pendonor->phone}}</span></li>
                                    <li>Profesi<span>{{ucwords($data_pendonor->religion)}}</span></li>
                                    <li>Alamat<span>{{$data_pendonor->address}}</span></li>
                                </ul>
                            </div>

                            <div class="text-center">
                                <a href="/donor/{{$data_pendonor->id}}/edit" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="fa fa-pencil" aria-hidden="true"></i> Edit Profile 
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="profile-right">
                        <!-- Button to trigger modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#bloodDonorModal">
                            Tambah kegiatan
                        </button>
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title"><strong>Daftar Kegiatan</strong></h3>
                            </div>
                            <div class="panel-body">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>NAMA</th>
                                            <th>TANGGAL</th>
                                            <th>LOKASI</th>
                                            <th>STATUS</th>
                                            <th>AKSI</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data_pendonor->donorevent as $event)
                                        <!-- $data_pendonor->donorevent. donorevent is relation function in the donor model-->
                                        <tr>
                                            <td>{{$event->name}}</td>
                                            <td>{{date('d-m-Y', strtotime($event->date))}}</td>
                                            <td>{{$event->location}}</td>
                                            <td>
                                                <!-- using ajax from https://vitalets.github.io/x-editable. data-url in routes/api.php -->
                                                <a href="#" class="status" data-type="select" data-pk="{{$event->id}}" data-url="/api/donor/{{$data_pendonor->id}}/editstatus" data-title="Ubah status..."> {{$event->pivot->status}}</a>
                                            </td>
                                            <td>
                                                <a href="/donor/{{$data_pendonor->id}}/{{$event->id}}/deleteevent" class="btn btn-danger btn-sm" onClick="return confirm('Yakin mau dihapus')"data-toggle="tooltip" data-placement="top" title="Hapus">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END TABBED CONTENT -->
                        <div class="panel" >
                            <div id="chartNilai"></div>
                        </div>
                    </div>
                <!-- </div> -->
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="bloodDonorModal" tabindex="-1" role="dialog" aria-labelledby="noStaticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="noStaticBackdropLabel"><strong>Tambah Kegiatan</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/donor/{{$data_pendonor->id}}/addevent" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="donoreventId">Nama Kegiatan</label>
                        <!-- label for="donoreventId" harus sama dengan select name="donoreventId" dan nama yg dipanggil di controller. kalau enggak gak akan menampilkan donorevent dan gak tersimpan di db, karena label yg dipih tidak tersambung dengan select -->
                        <select class="form-control" id="exampleFormControlSelect1" name="donoreventId"> 
                            @foreach($data_event as $events)
                            <div {{($loop->last)}} >
                                <option value="{{$events->id}}" >{{$events->name}}</option>
                            </div>                         
                            @endforeach
                        </select>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
@section('footer')
<script src="{{asset('admin/assets/vitaletsEditable/js/bootstrap-editable.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $('.status').editable({
            source: [
                {value: 'processing', text: 'Masih proses'},
                {value: 'passed', text: 'Lolos'},
                {value: 'reject', text: 'Tidak lolos'}
            ]
        });
    });
</script>
@stop
