@extends('layouts.master')

@section('header')
<title>Edit Pendonor | PRPM Walen - Pimpinan Ranting Pemuda Muhammadiyah</title>
@stop

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            @if(session('success'))
                <div class="alert alert-success" role="alert">
                    {{session('success')}} 
                </div>
			@endif
            @if(session('errors'))
                <div class="alert alert-danger" role="alert">
                    {{session('errors')}}
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Edit Identitas Pendonor</strong></h3>
                        </div>
                        <div class="panel-body">
                            <form action="/donor/{{$data_pendonor->id}}/donoredit" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="form-group col-md-4 {{$errors->has('fullname') ? ' has-error' : ''}}">
                                        <label for="fullname">Nama Lengkap</label>
                                        <input name="fullname" type="text" class="form-control" id="fullname" aria-describedby="emailHelp" placeholder="Nama lengkap..." value="{{$data_pendonor->fullname}}">
                                        @if($errors->has('fullname'))
                                            <span class="help-block">{{$errors->first('fullname')}}</span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-4 {{$errors->has('nik') ? ' has-error' : ''}}">
                                        <label for="nik">NIK (Nomor Induk Kependudukan)</label>
                                        <input name="nik" type="text" class="form-control" id="nik" minlength="16" maxlength="16" aria-describedby="emailHelp" placeholder="330913..." value="{{$data_pendonor->nik}}">
                                        @if($errors->has('nik'))
                                            <span class="help-block">{{$errors->first('nik')}}</span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-2 has-error">
                                        <label for="gender">Jenis Kelamin</label>
                                        <label class="fancy-radio">
                                            <input name="gender" value="l" @if($data_pendonor->gender == "l") checked @endif type="radio">
                                            <span><i></i>Laki-laki</span>
                                        </label>
                                        <label class="fancy-radio">
                                            <input name="gender" value="p" @if($data_pendonor->gender == "p") checked @endif type="radio">
                                            <span><i></i>Perempuan</span>
                                        </label>
                                        @if($errors->has('gender'))
                                            <span class="help-block">{{$errors->first('gender')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4 {{$errors->has('birth_place') ? ' has-error' : ''}}">
                                        <label for="exampleInputEmail1">Tempat Lahir</label>
                                        <input name="birth_place" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Boyolali..." value={{$data_pendonor->birth_place}}>
                                        @if($errors->has('birth_place'))
                                            <span class="help-block">{{$errors->first('birth_place')}}</span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-4 {{$errors->has('birth_date') ? ' has-error' : ''}}">
                                        <label for="exampleInputEmail1">Tanggal Lahir</label>
                                        <input name="birth_date" type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tanggal lahir..." value={{$data_pendonor->birth_date}}>
                                        @if($errors->has('birth_date'))
                                            <span class="help-block">{{$errors->first('birth_date')}}</span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="religion">Profesi</label>
                                        <input name="religion" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Profesi..." value={{$data_pendonor->religion}}>
                                        @if($errors->has('religion'))
                                            <span class="help-block">{{$errors->first('religion')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4 {{$errors->has('address') ? ' has-error' : ''}}">
                                        <label for="textAlamat">Alamat</label>
                                        <textarea name="address" class="form-control" id="textAlamat" placeholder="Walen RT ... RW ..., Desa ..., Kec. ..., Kab. ..." rows="3" >{{$data_pendonor->address}}</textarea>
                                        @if($errors->has('address'))
                                            <span class="help-block">{{$errors->first('address')}}</span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-4 {{$errors->has('phone') ? ' has-error' : ''}}">
                                        <label for="exampleInputEmail1">Nomor Handphone</label>
                                        <input name="phone" type="text" maxlength="14" minlength="10" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="0856..." value={{$data_pendonor->phone}}>
                                        @if($errors->has('phone'))
                                            <span class="help-block">{{$errors->first('phone')}}</span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="golonganDarah">Golongan Darah</label>
                                        <select name="blood_group" class="form-control" id="golonganDarah">
                                            <option value="o" @if($data_pendonor->blood_group == "o") selected @endif>O</option>
                                            <option value="a" @if($data_pendonor->blood_group == "a") selected @endif>A</option>
                                            <option value="b" @if($data_pendonor->blood_group == "b") selected @endif>B</option>
                                            <option value="ab" @if($data_pendonor->blood_group == "ab") selected @endif>AB</option>
                                            <option value="belum" @if($data_pendonor->blood_group == "belum") selected @endif>Belum Tahu</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="custom-file {{$errors->has('avatar') ? ' has-error' : ''}}">
                                        <label for="customFile">Foto profil</label>
                                        <input type="file" name="avatar" class="custom-file-label" id="customFile">
                                        <small class="form-text text-muted">Foto harus berformat jpg, jpeg atau png</small>
                                        @if($errors->has('avatar'))
                                            <span class="help-block">{{$errors->first('avatar')}}</span>
                                        @endif
                                </div>
                                <br>
                                <button type="submit" class="btn btn-warning">Update</button>
                                <a href="javascript:history.back()" class="btn btn-default" >Batal</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
