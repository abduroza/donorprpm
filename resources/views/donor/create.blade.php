@extends('layouts.master')

@section('header')
<title>Buat Pendonor Baru | PRPM Walen - Pimpinan Ranting Pemuda Muhammadiyah</title>
@stop

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            @if(session('success'))
                <div class="alert alert-success" role="alert">
                    {{session('success')}} 
                </div>
			@endif
            <!-- yg error kayaknya gak ngaruh/jalan -->
            @if(session('errors'))
                <div class="alert alert-danger" role="alert">
                    {{session('errors')}}
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Masukkan Identitas Pendonor</strong></h3>
                        </div>
                        <div class="panel-body">
                            <form action="/donor/donorcreate" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="form-group col-md-4 {{$errors->has('fullname') ? ' has-error' : ''}}">
                                        <label for="fullname">Nama Lengkap</label>
                                        <input name="fullname" type="text" class="form-control" id="fullname" aria-describedby="emailHelp" placeholder="Nama lengkap..." value="{{old('fullname')}}">
                                        <small class="form-text text-muted">Minimal 2 karakter</small>
                                        @if($errors->has('fullname'))
                                            <span class="help-block">{{$errors->first('fullname')}}</span>
                                        @endif
                                    </div>
                                    {{-- old() berguna untuk menampilkan kembali isian yg telah diiskan jika setelah tombol submit ditekan, tetapi gagal menambahkan data baru. sehingga client tinggal edit saja isian yg salah tidak perlu menulis form kosong lagi--}}
                                    <div class="form-group col-md-4 {{$errors->has('nik') ? ' has-error' : ''}}">
                                        <label for="nik">NIK (Nomor Induk Kependudukan)</label>
                                        <input name="nik" type="text" class="form-control" id="nik" minlength="16" maxlength="16" aria-describedby="emailHelp" placeholder="330913..." value="{{old('nik')}}">
                                        <small class="form-text text-muted">Angka 16 digit</small>
                                        @if($errors->has('nik'))
                                            <span class="help-block">{{$errors->first('nik')}}</span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-2 has-error">
                                        <label for="gender">Jenis Kelamin</label>
                                        <label class="fancy-radio">
                                            <input name="gender" value="l" {{old("gender") == "l" ? "checked" : ""}} type="radio">
                                            <span><i></i>Laki-laki</span>
                                        </label>
                                        <label class="fancy-radio">
                                            <input name="gender" value="p" {{old("gender") == "p" ? "checked" : ""}} type="radio">
                                            <span><i></i>Perempuan</span>
                                        </label>
                                        @if($errors->has('gender'))
                                            <span class="help-block">{{$errors->first('gender')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4 {{$errors->has('birth_place') ? ' has-error' : ''}}">
                                        <label for="exampleInputEmail1">Tempat Lahir</label>
                                        <input name="birth_place" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Boyolali..." value="{{old('birth_place')}}">
                                        @if($errors->has('birth_place'))
                                            <span class="help-block">{{$errors->first('birth_place')}}</span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-4 {{$errors->has('birth_date') ? ' has-error' : ''}}">
                                        <label for="exampleInputEmail1">Tanggal Lahir</label>
                                        <input name="birth_date" type="date" min='1945-01-01' max="{{date('Y-m-d')}}" class="form-control datepicker" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tanggal lahir..." value="{{old('birth_date')}}">
                                        <small class="form-text text-muted">Bulan / tanggal / tahun</small>
                                        @if($errors->has('birth_date'))
                                            <span class="help-block">{{$errors->first('birth_date')}}</span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="religion">Profesi</label>
                                        <input name="religion" type="text" class="form-control" id="religion" aria-describedby="emailHelp" placeholder="330913..." value="{{old('religion')}}">
                                        <small class="form-text text-muted">Angka 16 digit</small>
                                        @if($errors->has('religion'))
                                            <span class="help-block">{{$errors->first('religion')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4 {{$errors->has('address') ? ' has-error' : ''}}">
                                        <label for="textAlamat">Alamat</label>
                                        <textarea name="address" class="form-control" id="textAlamat" placeholder="Walen RT ... RW ..., Desa ..., Kec. ..., Kab. ..." rows="3" >{{old('address')}}</textarea>
                                        @if($errors->has('address'))
                                            <span class="help-block">{{$errors->first('address')}}</span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-4 {{$errors->has('phone') ? ' has-error' : ''}}">
                                        <label for="exampleInputEmail1">Nomor Handphone</label>
                                        <input name="phone" type="text" maxlength="14" minlength="10" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="0856..." value="{{old('phone')}}">
                                        @if($errors->has('phone'))
                                            <span class="help-block">{{$errors->first('phone')}}</span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="golonganDarah">Golongan Darah</label>
                                        <select name="blood_group" class="form-control" id="golonganDarah">
                                            <option value="o" {{(old("blood_group") == "o") ? "selected" : ""}}>O</option>
                                            <option value="a" {{(old("blood_group") == "a") ? "selected" : ""}}>A</option>
                                            <option value="b" {{(old("blood_group") == "b") ? "selected" : ""}}>B</option>
                                            <option value="ab" {{(old("blood_group") == "ab") ? "selected" : ""}}>AB</option>
                                            <option value="belum" {{(old("blood_group") == "belum") ? "selected" : ""}}>Belum Tau</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="custom-file {{$errors->has('avatar') ? ' has-error' : ''}}">
                                    <label for="customFile">Foto profil</label>
                                    <input type="file" name="avatar" class="custom-file-label" id="customFile">
                                    <small class="form-text text-muted">Foto berformat jpg, jpeg atau png</small>
                                    @if($errors->has('avatar'))
                                        <span class="help-block">{{$errors->first('avatar')}}</span>
                                    @endif
                                </div>
                                <br>
                                <button type="submit" class="btn btn-primary">Tambah</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
