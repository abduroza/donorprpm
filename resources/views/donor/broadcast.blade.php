@extends('layouts.master')
@section('header')
<title>Pendonor | PRPM Walen - Pimpinan Ranting Pemuda Muhammadiyah</title>
@stop

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            @if(session('success'))
                <div class="alert alert-success" role="alert">
                    {{session('success')}} 
                </div>
			@endif
            @if(session('error'))
                <div class="alert alert-danger" role="alert">
                    {{session('error')}}
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Broadcast Pendaftaran Donor Darah</strong></h3>
                        </div>
                        <div class="panel-body">
                            {{-- <form action="/donor/broadcast" method="POST">
                                {{csrf_field()}}
                                <div class="form-group " {{$errors->has('content') ? ' has-error' : ''}}">
                                    <label for="content">Konten</label>
                                    <textarea name="content" placeholder="Isikan text yang akan dikirimkan..." id="content" class="form-control" cols="3" rows="10">{{old('content')}}</textarea>
                                    @if($errors->has('content'))
                                        <span class="help-block">{{$errors->first('content')}}</span>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-primary">Kirim</button>
                            </form> --}}
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Alamat</th>
                                        <th>No. HP</th>
                                        <th>Status</th>
                                        <th>Kirim</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no_urut = 1;
                                    @endphp
                                    @foreach($data_pendonor as $pendonor)
                                    <tr>
                                        <td>{{$no_urut}}. </td>
                                        <td>
                                            <a href="/donor/{{$pendonor['id']}}/profile">
                                            {{$pendonor['fullname']}}-
                                            </a>
                                        </td>
                                        <td>{{explode(' ', trim($pendonor['address']))[0]}} </td>
                                        <td>
                                            <a href="{{$pendonor['phone']}}" target="_blank">
                                                {{$pendonor['phone']}}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $pendonor['status'] }}
                                        </td>
                                        <td>
                                            <form action="/donor/broadcast" method="POST">
                                                {{csrf_field()}}
                                                <div class="form-group " {{$errors->has('content') ? ' has-error' : ''}}">
                                                    <textarea name="content" placeholder="Isikan text yang akan dikirimkan..." id="content" class="form-control" cols="5" rows="5">{{old('content')}}</textarea>
                                                    @if($errors->has('content'))
                                                        <span class="help-block">{{$errors->first('content')}}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group" {{$errors->has('phone') ? ' has-error' : ''}}">
                                                    <input name="phone" type="text" class="form-control" id="phone" aria-describedby="phone" placeholder="No. HP" value="{{$pendonor['phone']}}" >
                                                    @if($errors->has('content'))
                                                        <span class="help-block">{{$errors->first('phone')}}</span>
                                                    @endif
                                                </div>
                                                <button type="submit" class="btn btn-primary">Kirim</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @php
                                        $no_urut ++;
                                    @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
