<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('/frontend')}}/scss/main.css">
    <link rel="stylesheet" href="{{asset('/frontend')}}/scss/skin.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('/frontend')}}/./script/index.js"></script>
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('admin/assets/img/faviconn.png')}}">
    @yield('header')
</head>

<body id="wrapper">
    <header>
        <nav class="navbar navbar-inverse">
            <div class="container">
                <div class="row">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/">
                            <h1>PRPM</h1><span>Fastabiqul Khairat</span>
                        </a>
                    </div>
                    <div id="navbar" class="collapse navbar-collapse navbar-right">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="/">Home</a></li>
                            <li><a href="/dashboard">Donor darah</a></li>
                            <li><a href="/gallery">Galeri</a></li>
                            <li><a href="/about">Tentang</a></li>
                            <li><a href="/contact">Kontak</a></li>
                            @if(auth()->user() != null)
                                <li><a href="/logout">Logout</a></li>
                            @else
                                <li><a href="/login">Login</a></li>
                            @endif
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </nav>
    </header>
    <!--/.nav-ends -->

    @yield('content')

    <section id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12 block">
                    <div class="footer-block">
                        <h4>Alamat</h4>
                        <hr/>
                        <p>Jl. Singoprono Raya KM. 04 Walen, Kec. Simo, Kab. Boyolali (Depan MI Muhammadiyah 1 Walen)</p>
                        <p>Telp: 0878 3642 0202</p>
                        <a href="/contact" class="learnmore">Selengkapnya <i class="fa fa-caret-right"></i></a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 block">
                    <div class="footer-block">
                        <h4>Link</h4>
                        <hr/>
                        <ul class="footer-links">
                            <li><a href="/about">Tentang Kami</a></li>
                            <li><a href="/contact">Kontak</a></li>
                            <li><a href="/login">Login</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 <block></block>">
                    <div class="footer-block">
                        <h4>Kegiatan terakhir</h4>
                        <hr/>
                        <ul class="footer-links">
                            <li>
                                <a href="#" class="post">Donor darah</a>
                                <p class="post-date">5 Januari 2020</p>
                            </li>
                            <li>
                                <a href="#" class="post">Pengajian akbar</a>
                                <p class="post-date">24 Maret 2020</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="bottom-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 btm-footer-links">
                    <a href="#">Kebijakan Privasi</a>
                    <a href="#">Syarat Penggunaan</a>
                </div>
            </div>
        </div>
    </section>
    <div id="panel">
        <div id="panel-admin">
            <div class="panel-admin-box">
                <div id="tootlbar_colors">
                    <button class="color" style="background-color:#1abac8;" onclick="mytheme(0)"></button>
                    <button class="color" style="background-color:#ff8a00;" onclick="mytheme(1)"> </button>
                    <button class="color" style="background-color:#b4de50;" onclick="mytheme(2)"> </button>
                    <button class="color" style="background-color:#e54e53;" onclick="mytheme(3)"> </button>
                    <button class="color" style="background-color:#1abc9c;" onclick="mytheme(4)"> </button>
                    <button class="color" style="background-color:#159eee;" onclick="mytheme(5)"> </button>
                </div>
            </div>
        </div>
        <a class="open" href="#"><span><i class="fa fa-gear fa-spin"></i></span></a>
    </div>
</html>
