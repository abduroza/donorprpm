<div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll">
        <nav>
            <ul class="nav">
                <li>
                    <a href="/dashboard" class="{{ $page == 'dashboard' ? 'active' : '' }}">
                        <i class="lnr lnr-home"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="/user" class="{{ $page == 'user' ? 'active' : '' }}"><i class="lnr lnr-user"></i> <span>User</span></a>
                </li>
                <li>
                    <a href="/donor" class="{{ $page == 'donor' ? 'active' : '' }}">
                    <i class="lnr lnr-users"></i> <span>Pendonor</span>
                    </a>
                </li>
                <li>
                    <a href="/donorevent" class="{{ $page == 'event' ? 'active' : '' }}"><i class="lnr lnr-briefcase"></i> <span>Kegiatan</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>
