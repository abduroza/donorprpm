<nav class="navbar navbar-default navbar-fixed-top" style="position: absolute">
    <div class="brand">
        <a href="/"><img src="{{asset('admin/assets/img/logo-dark.png')}}" alt="PRPM Logo" class="img-responsive logo"></a>
    </div>
    <div class="container-fluid">
        <div class="navbar-btn">
            <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
        </div>
        <form action="/{{Route::current()->uri()}}" class="navbar-form navbar-left" method="GET">
            {{-- <div class="input-group">
                <input name="search" value="" class="form-control" placeholder="Cari...">
                <span class="input-group-btn"><button type="submit" class="btn btn-primary">Cari</button></span>
            </div> --}}
        </form>
        <div id="navbar-menu">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{(auth()->user()->donor == null) ? asset('images/avatarDefault.png') : getAvatar(auth()->user()->donor->id)}}" class="img-circle" alt="Avatar">
                        <span>{{auth()->user()->fullname}}</span>
                        <i class="icon-submenu lnr lnr-chevron-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        @if(auth()->user()->donor != null)
                        <li>
                            <a href="/donor/{{auth()->user()->donor->id}}/profile">
                                <i class="lnr lnr-user"></i>
                                <span>My Profile</span>
                            </a>
                        </li>
                        @endif
                        <li>
                            <a href="/logout">
                                <i class="lnr lnr-exit"></i>
                                <span>Logout</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

