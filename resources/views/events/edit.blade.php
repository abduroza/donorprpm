@extends('layouts.master')

@section('header')
<title>Edit Kegiatan | PRPM Walen - Pimpinan Ranting Pemuda Muhammadiyah</title>
@stop

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            @if(session('success'))
                <div class="alert alert-success" role="alert">
                    {{session('success')}} 
                </div>
			@endif
            <!-- yg error kayaknya gak ngaruh/jalan -->
            @if(session('errors'))
                <div class="alert alert-danger" role="alert">
                    {{session('errors')}} 
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Edit Kegiatan</strong></h3>
                        </div>
                        <div class="panel-body">
                            <form action="/donorevent/{{$donor_event->id}}/eventedit" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="form-group col-md-4 {{$errors->has('name') ? ' has-error' : ''}}">
                                        <label for="name">Nama</label>
                                        <input name="name" type="text" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Donor Darah Pertama..." value="{{$donor_event->name}}">
                                        @if($errors->has('name'))
                                            <span class="help-block">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-4 {{$errors->has('date') ? ' has-error' : ''}}">
                                        <label for="date">Tanggal</label>
                                        <input name="date" type="date" min="{{date('Y-m-d')}}" class="form-control"  id="date" aria-describedby="emailHelp" placeholder="Masukkan tanggal..." value="{{$donor_event->date}}">
                                        <small class="form-text text-muted">Bulan / tanggal / tahun</small>
                                        @if($errors->has('date'))
                                            <span class="help-block">{{$errors->first('date')}}</span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-4 {{$errors->has('location') ? ' has-error' : ''}}">
                                        <label for="location">Tempat</label>
                                        <input name="location" type="text" class="form-control" id="location" aria-describedby="emailHelp" placeholder="Masukkan tempat kegiatan..." value="{{$donor_event->location}}">
                                        @if($errors->has('location'))
                                            <span class="help-block">{{$errors->first('location')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-warning">Update</button>
                                <a href="javascript:history.back()" class="btn btn-default" >Batal</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
