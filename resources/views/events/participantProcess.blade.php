@extends('layouts.master')

@section('header')
<title>Peserta Proses | PRPM Walen - Pimpinan Ranting Pemuda Muhammadiyah</title>
@stop

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            @if(session('success'))
                <div class="alert alert-success" role="alert">
                    {{session('success')}}
                </div>
			@endif
            @if(session('errors'))
                <div class="alert alert-danger" role="alert">
                    {{session('errors')}}
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Peserta Dalam Proses {{ $name_date->name }} -- {{ date('d-m-Y', strtotime($name_date->date)) }}</strong></h3>
                            <div class='right'>
                                {{-- <ul class="nav navbar-right">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <span>Download</span>
                                            <i class="icon-submenu lnr lnr-chevron-down"></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="/donorevent/processing/{{$name_date->id}}/excel">
                                                    <i class="lnr lnr-download"></i>
                                                    <span>Excel</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul> --}}
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-hover" id="datatable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Tanggal Lahir</th>
                                        <th>No. HP</th>
                                        <th>Profesi</th>
                                        <th>Goldar</th>
                                        <th>Alamat</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no_urut = 1;
                                    @endphp
                                    @foreach($data_event as $pendonor)
                                    <tr>
                                        <td>{{$no_urut}}</td>
                                        <td>
                                            <a href="/donor/{{$pendonor->id}}/profile">{{$pendonor->fullname}}</a>
                                        </td>
                                        <td>{{date('d-m-Y', strtotime($pendonor->birth_date))}}</td>
                                        <td>{{$pendonor->phone}}</td>
                                        <td>{{$pendonor->religion}}</td>
                                        <td>{{strtoupper($pendonor->blood_group)}}</td>
                                        <td>{{$pendonor->address}}</td>
                                        <td>
                                            <a href="/donorevent/{{$name_date->id}}/{{$pendonor->id}}/participantloloskan" class="btn btn-success btn-sm" tabindex="-1" aria-disabled="false" data-toggle="tooltip" data-placement="top" title="Loloskan">
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                            </a>
                                            <a href="/donorevent/{{$name_date->id}}/{{$pendonor->id}}/participantTolak" class="btn btn-danger btn-sm" tabindex="-1" aria-disabled="false" data-toggle="tooltip" data-placement="top" title="Tolak">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @php
                                        $no_urut ++;
                                    @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
    <script>
        $(document).ready(function(){ //berarti akan dijalankan setelah semua dokumen diload
            $('#datatable').DataTable()
        })
    </script>
@endsection
