@extends('layouts.master')

@section('header')
<title>Kegiatan | PRPM Walen - Pimpinan Ranting Pemuda Muhammadiyah</title>
@stop

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            @if(session('success'))
                <div class="alert alert-success" role="alert">
                    {{session('success')}} 
                </div>
			@endif
            @if(session('errors'))
                <div class="alert alert-danger" role="alert">
                    Gagal membuat kegiatan baru...
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Daftar Kegiatan Donor Darah</strong></h3>
                            <div class='right'>
                                <a href="/donorevent/eventcreate" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#staticBackdrop">Tambah Kegiatan Donor</a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover" id="datatable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Tanggal</th>
                                        <th>Lokasi</th>
                                        <th>Peserta</th>
                                        <th>Proses</th>
                                        <th>Lolos</th>
                                        <th>Tidak Lolos</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no_urut = 1;
                                    @endphp
                                    @foreach($donor_events as $event)
                                    <tr>
                                        <td>{{$no_urut}}</td>
                                        <td>{{$event->name}}</td>
                                        <td>{{date('l, d-m-Y', strtotime($event->date))}}</td>
                                        <td>{{$event->location}}</td>
                                        <td>
                                            <a href="/donorevent/{{$event->id}}/participant">{{$event->donorEventTotalJoin()}}</a>
                                        </td>
                                        <td>
                                            <a href="/donorevent/{{$event->id}}/participantprocessing">{{$event->donorEventTotalProcessing()}}</a>
                                        </td>
                                        <td>
                                            <a href="/donorevent/{{$event->id}}/participantpassed">{{$event->donorEventTotalPassed()}}</a>
                                        </td>
                                        <td>
                                            <a href="/donorevent/{{$event->id}}/participantreject">{{$event->donorEventTotalReject()}}</a>
                                        </td>
                                        <td>
                                            <a href="/donorevent/{{$event->id}}/edit" class="btn btn-warning btn-sm" tabindex="-1" aria-disabled="false" data-toggle="tooltip" data-placement="top" title="Edit">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>
                                            <a href="/donorevent/{{$event->id}}/eventdelete" class="btn btn-danger btn-sm" tabindex="-1" aria-disabled="false" onClick="return confirm('Yakin mau dihapus')" data-toggle="tooltip" data-placement="top" title="Hapus">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @php
                                        $no_urut ++;
                                    @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel"><strong>Buat Kegiatan Baru</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/donorevent/eventcreate" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group {{$errors->has('name') ? ' has-error' : ''}}">
                    <label for="name">Nama</label>
                        <input name="name" type="text" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Donor Darah Pertama..." value="{{old('name')}}">
                        @if($errors->has('name'))
                            <span class="help-block">{{$errors->first('name')}}</span>
                        @endif
                    </div>
                    <div class="form-group {{$errors->has('date') ? ' has-error' : ''}}">
                        <label for="date">Tanggal</label>
                        <input name="date" type="date" class="form-control" min="{{date('Y-m-d')}}" id="date" aria-describedby="emailHelp" placeholder="Masukkan tanggal..." value="{{old('date')}}">
                        <small class="form-text text-muted">Bulan / tanggal / tahun</small>
                        @if($errors->has('date'))
                            <span class="help-block">{{$errors->first('date')}}</span>
                        @endif
                    </div>
                    <div class="form-group {{$errors->has('location') ? ' has-error' : ''}}">
                        <label for="location">Tempat</label>
                        <input name="location" type="text" class="form-control" id="location" aria-describedby="emailHelp" placeholder="Masukkan tempat kegiatan..." value="{{old('location')}}">
                        @if($errors->has('location'))
                            <span class="help-block">{{$errors->first('location')}}</span>
                        @endif
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Buat</button>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
    <script>
        $(document).ready(function(){ //berarti akan dijalankan setelah semua dokumen diload
            $('#datatable').DataTable()
        })
    </script>
@endsection