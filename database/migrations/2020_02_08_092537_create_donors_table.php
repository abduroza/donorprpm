<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fullname')->nullable();
            $table->string('nik', 45)->unique();
            $table->string('gender');
            $table->string('birth_place', 255);
            $table->date('birth_date')->nullable();
            $table->string('phone', 45)->unique()->nullable();
            $table->string('religion', 255)->nullable();
            $table->string('blood_group', 45)->nullable();    //enum('blood_group', ['a', 'b', 'ab', 'o'])->nullable();
            $table->text('address');
            $table->string('avatar', 255)->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donors');
    }
}
