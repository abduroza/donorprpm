<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonorDonoreventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donor_donorevent', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('status', ['processing', 'passed', 'reject'])->default('processing');
            $table->integer('donor_id')->nullable();
            $table->integer('donorevent_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donor_donorevent');
    }
}
