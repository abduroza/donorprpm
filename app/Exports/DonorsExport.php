<?php

namespace App\Exports;

use App\Donor;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping; //untuk custom keluarannya
use Maatwebsite\Excel\Concerns\WithHeadings; // for add heading in output
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\ShouldAutoSize; //to perform an automatic width calculation
use PhpOffice\PhpSpreadsheet\Style\NumberFormat; //format number
use Maatwebsite\Excel\Concerns\WithColumnFormatting; //to format number in column
use \Carbon\Carbon;

class DonorsExport implements FromCollection, WithMapping, WithHeadings, WithColumnFormatting,
ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Donor::all();
    }

    // memodifikasi kolom
    public function map($donor): array
    {
        $no_urut = 0;
        // for($a = 1; $a <= 10; $a++){
        //     $no_urut = $a;
        // };
        return [
            $no_urut,
            $donor->fullname,
            $donor->nik,
            strtoupper($donor->blood_group),
            $donor->donorEventTotalPassed(),
            strtoupper($donor->gender),
            ucwords($donor->birth_place),
            date('d-m-Y', strtotime($donor->birth_date)),
            getAge($donor->id), // getAge() is function that made in app/Helpers/Global.php
            $donor->phone,
            ucwords($donor->religion),
            ucwords($donor->address),
            Date::dateTimeToExcel($donor->created_at),
        ];
    }

    //menambahkan heading pada kolom table
    public function headings(): array
    {
        return [
            [
                'DAFTAR PENDONOR DARAH PRPM WALEN'
            ],
            [
                'Per Tanggal ' . date('d-m-Y', strtotime(Carbon::today()))
            ],
            [
                ' '
            ],
            [
                'NO',
                'NAMA',
                'NIK',
                'GOLDAR',
                'TOTAL DONOR',
                'JENIS KELAMIN',
                'KELAHIRAN',
                'TGL LAHIR',
                'UMUR',
                'NO. HP',
                'PROFESI',
                'ALAMAT',
                'TANGGAL TERDAFTAR'
            ]
        ];
    }

    //format tanggal dan nomor
    public function columnFormats(): array
    {
        return [
            'M' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            // 'C' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
        ];
    }
}
