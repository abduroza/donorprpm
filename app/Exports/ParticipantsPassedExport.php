<?php

namespace App\Exports;

use App\Donorevent;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping; //untuk custom keluarannya
use Maatwebsite\Excel\Concerns\WithHeadings; // for add heading in output
use Maatwebsite\Excel\Concerns\ShouldAutoSize; //to perform an automatic width calculation

class ParticipantsPassedExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    protected $event_id;

    function __construct($event_id) {
        $this->event_id = $event_id;
    }

    public function collection()
    {
        return Donorevent::find($this->event_id)->donor()->where('status', 'passed')->get();
        
    }
    
    // memodifikasi kolom
    public function map($donor): array
    {
        $no_urut = 0;

        return [
            $no_urut,
            $donor->fullname,
            strtoupper($donor->blood_group),
            $donor->donorEventTotalPassed(),
            strtoupper($donor->gender),
            $donor->birth_place,
            date('d-m-Y', strtotime($donor->birth_date)),
            getAge($donor->id), // getAge() is function that made in app/Helpers/Global.php
            $donor->phone,
            ucwords($donor->religion),
            ucwords($donor->address),
        ];
    }

    //menambahkan heading pada kolom table
    public function headings(): array
    {

        //get the data event to show date
        $date_event = Donorevent::find($this->event_id);
        return [
            [
                'DAFTAR PESERTA LOLOS DONOR DARAH PRPM WALEN'
            ],
            [
                'Tanggal ' . date('d-m-Y', strtotime($date_event->date))
            ],
            [
                ' '
            ],
            [
                'NO',
                'NAMA',
                'GOLDAR',
                'TOTAL DONOR',
                'JENIS KELAMIN',
                'KELAHIRAN',
                'TGL LAHIR',
                'UMUR',
                'NO. HP',
                'PROFESI',
                'ALAMAT',
            ]
        ];
    }

}
