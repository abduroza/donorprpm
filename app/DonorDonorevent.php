<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonorDonorevent extends Model
{
    protected $fillable = ['donor_id', 'donorevent_id', 'status'];
    protected $table = ['donor_donorevent']; // to ensure table nama is donor_donorevent not donor_donorevents
}
