<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donorevent extends Model
{
    protected $fillable = ['name', 'date', 'location', 'status', 'user_id', 'donor_id'];

    //relation
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function donor(){
        return $this->belongsToMany(Donor::class)->withPivot(['status'])->withTimestamps();
    }

    //helpers
    //this function to show in /events/index.blade.php
    //count all participants that join the event
    function donorEventTotalJoin(){
        // dd($this->donor()->count());
        return $this->donor()->count();
    }

    function donorEventTotalProcessing(){
        return $this->donor()->where('status', 'processing')->count();
    }

    //function to count passed participant
    function donorEventTotalPassed(){
        return $this->donor()->where('status', 'passed')->count();
    }

    //function to count reject participant
    function donorEventTotalReject(){
        return $this->donor()->where('status', 'reject')->count();
    }
}
