<?php

use \App\Donor;
use \App\Donorevent;
use \Carbon\Carbon;

//get age using donor table. Need id donor as parameter
function getAge($id){
    $donor = Donor::find($id)->birth_date;
    return (Carbon::parse($donor)->age);
}

//show donor profile image. Need id donor as parameter
function getAvatar($id){
    $donor = Donor::find($id);
    if(!$donor->avatar){
        return asset('images/avatarDefault.png');
    }
    return asset('images/'.$donor->avatar);
}

//widget dashboard
function donorTotal(){
    return Donor::count();
}

function donorEventTotal(){
    return Donorevent::count();
}

function donorEventLast(){
    return Donorevent::get()->last()->date;
}

function ranks(){
    $donors = Donor::all();
    //using func map() will make new virtual column in table donors with name e
    $donors->map(function($e){
        $e->most = $e->donorEventTotalPassed(); //donorEventTotalPassed() is function in Donor model
        return $e;
    });
    $mostDonors = $donors->sortByDesc('most')->take(6);
    return $mostDonors;
}

//count blood group to make pie chart. Not change to percent, because chart convert to percent automatically. Only need amount group blood
function bloodA(){
    $a = Donor::whereIn('blood_group', ['a'])->count();
    $bloodTotal = Donor::count();
    ($a == 0) ? $percent = 0 : $percent = $a / $bloodTotal * 100;
    return $a;
}

function bloodB(){
    $b = Donor::whereIn('blood_group', ['b'])->count();
    return $b;
}

function bloodAB(){
    $ab = Donor::whereIn('blood_group', ['ab'])->count();
    return $ab;
}

function bloodO(){
    $o = Donor::whereIn('blood_group', ['o'])->count();
    return $o;
}

function bloodBelum(){
    $belum = Donor::whereIn('blood_group', ['belum'])->count();
    return $belum;
}






