<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$role) //... mean, input is array from route
    {
        if(in_array($request->user()->role, $role)){
            return $next($request);
        }
        return redirect()->back()->with('error', 'Maaf anda tidak diizinkan untuk mengaksesnya');
        // redirect()->back() means stay on current page
    }
}
