<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function editStatus(Request $request, $donor_id){
        $donor = \App\Donor::find($donor_id);
        //$request->pk mengarah ke data-pk="{{$event->id}}"
        $donor->donorevent()->updateExistingPivot($request->pk, ['status' => $request->value]);
    }
}
