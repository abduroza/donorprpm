<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Donor;
use Auth;

class userController extends Controller
{
    public function index(Request $request)
    {
        $page = 'user';
        if($request->has('search')){
            $users = User::where('fullname', 'LIKE', '%' .$request->search. '%')
            ->orWhere('email', 'LIKE', '%' .$request->search. '%')
            ->orWhere('role', 'LIKE', '%' .$request->search. '%')       
            ->get();
            // ->paginate(20);
        } else {
            $users = User::all();
            // $users = User::paginate(20);
        }
        return view('user.index', ['page' => $page, 'data_user' => $users]);
    }

    //menampilkan halaman login
    public function register (){
        return view('auths.register');
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'fullname' => 'required|min:3',
            'email' => 'required|email|unique:users', //unique:users. berarti harus uniq di tabel users
        ],[
            'required' => ':attribute wajib diisi.',
            'min' => ':attribute minimal 3 karakter',
            'email' => 'masukkan nama :attribute yang benar!',
            'unique' => ':attribute sudah terdaftar. Silahkan gunakan :attribute yang lain!'
        ]);

        $password = '';
        $role = '';
        ($request->filled('password')) ? $password = bcrypt($request->password) : $password = bcrypt('12345');
        ($request->has('role')) ? $role = $request->role :  $role = 'donor';
        $user = User::create([
            'fullname' => $request['fullname'],
            'email' => $request['email'],
            'password' => $password,
            'role' => $role,
            'remember_token' => \Str::random(60),
        ]);

        return redirect('/login')->with('success', "Pendaftaran sukses");
    }

    //show user to edit
    public function edit($id){
        $page = 'user';
        $users = User::find($id);
        return view('user.edit', ['page' => $page, 'data_user' => $users]);
    }

    //update user data
    public function userEdit(Request $request, $id){
        $this->validate($request, [
            'fullname' => 'required|min:3',
            'email' => 'required'
        ],[
            'required' => ':attribute wajib diisi',
            'min' => ':attribute minimal 3 karakter'
        ]);

        $user = User::find($id);
        $user->fullname = $request->fullname;
        $user->email = $request->email;
        $user->role = $request->role;

        //also update fullname in user table, because in user table also have fullname field 
        $donor = Donor::find($user->donor->id);
        $donor->fullname = $request->fullname;

        if($request->password != null){
            $user->password =  bcrypt($request->password);
        }
        $user->save();
        $donor->save();

        return redirect('/user')->with('success', 'Data user ' .$user->fullname. ' berhasil diedit');
    }

    //delete user. only user. donor and donorevent not delete
    public function userDelete($user_id){ //$user_id parameter get from url /user/{user_id}/userdelete. See route
        $user = User::find($user_id);
        $user->delete($user);

        return redirect('/user')->with('success', 'Data user ' .$user->fullname. ' berhasil dihapus');
    }

    //show login page
    public function login(){
        return view('auths.login');
    }

    //handle login
    public function userLogin(Request $request){
        if(Auth::attempt($request->only('email', 'password'))){
            return redirect('/dashboard')->with('success', "Anda berhasil login");
        };
        return redirect('/login')->with('error', 'Email atau password salah');
        //'error' handle withput 's'. if using s will error in login.blade.php. I don't know why do
    }

    //handle logout
    public function logout(){
        Auth::logout();
        return redirect('/login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
