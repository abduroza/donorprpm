<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Donor;
use App\Donorevent;

class DashboardController extends Controller
{
    public function index(){

        $page = 'dashboard';
        $donorEvent = Donorevent::all()->sortByDesc('date')->take(7);
        $donors = Donor::all();

        // for participant chart
        $date = [];
        $participant = [];
        $passed = [];
        $reject = [];
        foreach ($donorEvent as $event) {
            $date[] = date('d-m-Y', strtotime($event->date)); //$date[]. using [] because result in array
            $participant[] = $event->donor()->count();
            $passed[] = $event->donor()->where('status', 'passed')->count();
            $reject[] = $event->donor()->where('status', 'reject')->count();
        };

        return view('dashboards.index', ['page' => $page, 'date' => $date, 'participant' => $participant, 'passed' => $passed, 'reject' => $reject]);
    }

    public function goldarA(){
        $page = 'dashboard';
        $goldar_a = Donor::where('blood_group', '=', 'a')->get();

        return view('dashboards.goldara', ['page' => $page, 'goldarA' => $goldar_a]);
    }

    public function goldarB(){
        $page = 'dashboard';
        $goldar_b = Donor::where('blood_group', '=', 'b')->get();

        return view('dashboards.goldarb', ['page' => $page, 'goldarB' => $goldar_b]);
    }

    public function goldarAB(){
        $page = 'dashboard';
        $goldar_ab = Donor::where('blood_group', '=', 'ab')->get();

        return view('dashboards.goldarab', ['page' => $page, 'goldarAB' => $goldar_ab]);
    }

    public function goldarO(){
        $page = 'dashboard';
        $goldar_o = Donor::where('blood_group', '=', 'o')->get();

        return view('dashboards.goldaro', ['page' => $page, 'goldarO' => $goldar_o]);
    }
}
