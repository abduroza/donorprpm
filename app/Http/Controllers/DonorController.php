<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use \App\Donor;
use \App\Donorevent;
use \App\DonorDonorevent;
use \Carbon\Carbon;
use GuzzleHttp\Client;

//export excel
use App\Exports\DonorsExport;
use Maatwebsite\Excel\Facades\Excel;

class DonorController extends Controller
{
    public function index(Request $request){

        $page = 'donor';

        if($request->has('search')){
            $donors = Donor::where('nik', 'LIKE', '%' .$request->search. '%')
            ->orWhere('fullname', 'LIKE', '%' .$request->search. '%')
            ->orWhere('address', 'LIKE', '%' .$request->search. '%')
            ->orWhere('phone', 'LIKE', '%' .$request->search. '%')
            ->orWhere('blood_group', 'LIKE', '%' .$request->search. '%')
            ->orWhere('religion', 'LIKE', '%' .$request->search. '%')          
            ->get();
        } else {
            $donors = Donor::all(); // atau Donor::get();
        }

        return view('donor.index', ['page' => $page, 'data_pendonor' => $donors]);
    }

    //create new donor
    public function create(){
        $page = 'donor';
        return view('donor.create', ['page' => $page]);
    }
    public function donorCreate(Request $request){

        $this->validate($request, [
            'fullname' => 'required|min:3',
            'email' => 'nullable|unique:users',
            'nik' => 'required|unique:donors',
            'gender' => 'required',
            'birth_place' => 'required',
            // 'phone' => 'unique:donors',
            'address' => 'required',
            'avatar' => 'mimes:jpg,png,jpeg',
        ],[
            'required' => ':attribute wajib diisi',
            'min' => ':attribute minimal 3 karakter',
            'email' => 'format :attribute harus benar',
            'unique' => ':attribute sudah terdaftar. Gunakan :attribute lain.',
            'mimes' => 'Gambar harus berformat jpg, png atau jpeg.'
        ]);

        //make random email
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $randomEmail = substr(str_shuffle($chars), 0, 7) . "@gmail.com";

        // add new user
        $user = new User;
        $user->fullname = $request->fullname;
        $user->email = $randomEmail;
        $user->password = bcrypt('12345');
        $user->role = 'donor';
        $user->remember_token = \Str::random(60);

        //add donor profile
        $donor = Donor::create($request->all());
        $user->save(); //this state place in here, so if table donor fail create, table user also can't create
        $donor->user_id = $user->id;
        if($request->hasFile('avatar')){
            $request->file('avatar')->move('images', $request->file('avatar')->getClientOriginalName());
            $donor->avatar = $request->file('avatar')->getClientOriginalName();
        }
        $donor->save();

        return redirect('/donor/'.$donor->id.'/profile')->with('success', 'Pendonor darah baru berhasil ditambahkan');
    }

    public function edit($id){
        $page = 'donor';
        $donors = Donor::find($id);
        return view('donor.edit', ['page' => $page, 'data_pendonor' => $donors]);
    }

    public function donorEdit(Request $request, $id){
        $this->validate($request, [
            'fullname' => 'required|min:3',
            'gender' => 'required',
            'birth_place' => 'required',
            'address' => 'required',
            'avatar' => 'mimes:jpg,png,jpeg',
        ],[
            'required' => ':attribute wajib diisi',
            'min' => ':attribute minimal 3 karakter',
            'mimes' => 'Gambar harus berformat jpg, png atau jpeg.'
        ]);

        $donor = Donor::find($id);
        $donor->update($request->all());

        //also update fullname in user table, because in user table also have fullname field 
        $user = User::find($donor->user_id);
        $user->fullname = $request->fullname;
        $user->save();

        if($request->hasFile('avatar')){
            $request->file('avatar')->move('images', $request->file('avatar')->getClientOriginalName());
            $donor->avatar = $request->file('avatar')->getClientOriginalName();
            $donor->save();
        }

        return redirect('/donor')->with('success', 'Data pendonor darah ' .$donor->fullname. ' berhasil diedit');
    }

    public function donorDelete($donor_id){ //$donor_id parameter get from url /donor/{donor_id}/donordelete. See route
        $donor = Donor::find($donor_id);
        $user = User::find($donor->user_id);
        $donorEvent = Donorevent::all();

        $donor->donorevent()->detach($donorEvent); //delete all event for specific donor_id
        $donor->delete($donor);
        $user->delete($user);

        return redirect('/donor')->with('success', 'Data pendonor darah ' .$donor->fullname. ' berhasil dihapus');
    }

    public function profile(Request $request, $id){
        $page = 'donor';
        if($request->has('search')){
            return abort(404,'Halaman tidak ditemukan');
        } else {
            $donor = Donor::find($id);
        }
        // $donor = Donor::find($id);
        $donorEvent = Donorevent::all()->sortByDesc('id'); ///this code used to sort array and display from last to early. used to fill donorEvent to an user in modal

        return view('donor.profile', ['page' => $page, 'data_pendonor' => $donor, 'data_event' => $donorEvent]);
    }

    public function addEvent(Request $request, $donor_id){
        $donor = Donor::find($donor_id);
        // $donorEvent = Donorevent::find($request->donoreventId);

        // if($donorEvent->date < Carbon::yesterday()){ // or Carbon::today()->subDay()
        //     return redirect('/donor/'.$donor_id.'/profile')->with('errors', 'Kegiatan sudah selesai. Pilih kegiatan lain');
        // }
        if($donor->donorevent()->where('donorevent_id', $request->donoreventId)->exists()){
            //$request->donoreventId) mengambil dari frontend
            return redirect('/donor/'.$donor_id.'/profile')->with('errors', 'Kegiatan sudah ditambahkan sebelumnya. Pilih kegiatan lain');
        };

        $donor->donorevent()->attach($request->donoreventId, ['status' => 'processing']); //donorevent() is function that declared in Donor model
        // atau pakai dibawah ini. sudah dicoba tapi belum berhasil. ErrorException Array to string conversion
        // $donor = DonorDonorevent::firstOrCreate([
        //     'donor_id' => $donor,
        //     'donorevent_id' => $request['donoreventId'],
        //     'status' => 'processing'
        // ]);

        return redirect('/donor')->with('success', 'Kegiatan berhasil ditambahkan ke '.$donor->fullname);
    }

    //delete event from an donor
    public function deleteEvent($donor_id, $donorevent_id){
        $donor = Donor::find($donor_id);
        $donor->donorevent()->detach($donorevent_id);
        return redirect('/donor/'.$donor_id.'/profile')->with('success', 'Kegiatan berhasil dihapus dari '.$donor->fullname);
    }

    // public function broadcastView1(Request $request)
    // {
    //     $donors = Donor::select('id', 'fullname', 'phone', 'address')->get();
    //     $data = [];
    //     foreach($donors as $donor){
    //         $data[] = [
    //             'id' => $donor->id,
    //             'fullname' => $donor->fullname,
    //             'phone' => 'https://api.whatsapp.com/send?phone=62' . substr($donor->phone, 1),
    //             'address' => $donor->address,
    //             'status' => '-'
    //         ];
    //     }
    //     return view('donor.broadcast', ['data_pendonor' => $data]);
    // }

    public function broadcastView(Request $request)
    {
        $page = 'donor';
        $donors = Donor::select('id', 'fullname', 'phone', 'address')->get();
        $data = [];
        foreach($donors as $donor){
            $data[] = [
                'id' => $donor->id,
                'fullname' => $donor->fullname,
                'phone' => $donor->phone,
                'address' => $donor->address,
                'status' => '-'
            ];
        }
        return view('donor.broadcast', ['page' => $page, 'data_pendonor' => $data]);
    }

    // public function broadcast1(Request $request)
    // {
    //     $this->validate($request, [
    //         'content' => 'required|min:20'
    //     ],[
    //         'required' => ':attribute wajib diisi',
    //         'min' => ':attribute minimal 20 karakter'
    //     ]);

    //     $client = new Client([
    //         // Base URI is used with relative requests
    //         'base_uri' => 'http://apiwabroadcast.herokuapp.com',
    //         // You can set any number of default request options.
    //         'timeout'  => 5.0,
    //     ]);

    //     $donors = Donor::select('id', 'fullname', 'phone', 'address')->get();
    //     $data = [];

    //     foreach ($donors as $donor) {
    //         $response = $client->request('POST', '/send-message/085647701900', [
    //             'http_errors' => false,
    //             'form_params' => [
    //                 'receiver' => $donor->phone,
    //                 'text' => $request->content
    //             ]
    //         ]);
    //         $res = json_decode($response->getBody()); //type data object
    //         $data[] = [
    //             'id' => $donor->id,
    //             'fullname' => $donor->fullname,
    //             'phone' => $donor->phone,
    //             'address' => $donor->address,
    //             'status' => $res->status == true ? 'Terkirim' : 'Gagal'
    //         ];
    //         usleep(mt_rand(1000000,4000000));
    //     }
    //     return view('donor.broadcast', ['data_pendonor' => $data]);
    // }
    public function broadcast(Request $request)
    {
        $this->validate($request, [
            'content' => 'required|min:20',
            'phone' => 'required'
        ],[
            'required' => ':attribute wajib diisi',
            'min' => ':attribute minimal 20 karakter'
        ]);

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'http://apiwabroadcast.herokuapp.com',
            // You can set any number of default request options.
            'timeout'  => 5.0,
        ]);

        $donor = Donor::where('phone', $request->phone)->first();

        $name = '';
        if(getAge($donor->id) > 40 ){
            if($donor->gender == 'l'){
                $name = 'Bapak ' .$donor->fullname;
            }elseif ($donor->gender == 'p') {
                $name = 'Ibu ' .$donor->fullname;
            }
        } else {
            if($donor->gender == 'l'){
                $name = 'Mas ' .$donor->fullname;
            }elseif ($donor->gender == 'p') {
                $name = 'Mbak ' .$donor->fullname;
            }
        }

        $response = $client->request('POST', '/send-message/085647701900', [
            'http_errors' => false,
            'form_params' => [
                'receiver' => '62' . substr($request->phone, 1),
                'text' => "Assalamu'alaikum " .$name. "\n\n" .$request->content
            ]
        ]);
        $res = json_decode($response->getBody()); //type data object

        $message = $res->status == true ? 'Berhasil mengirim whatsapp ke ' . $donor->fullname : 'Gagal mengirim whatsapp ke ' . $donor->fullname;
        $status = $res->status == true ? 'success' : 'error';

        return redirect('/donor/broadcast')->with($status, $message);
    }

    //export pendonor in excel format
    public function export() 
    {
        $fileName = 'daftarPendonor' . Carbon::now()->setTimezone('Asia/Jakarta')->format('d-m-Y H:i') . '.xlsx';
        return Excel::download(new DonorsExport, $fileName);
    }
}
