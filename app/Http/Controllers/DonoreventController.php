<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use \App\Donor;
use \App\Donorevent;

//export excel
use App\Exports\ParticipantsExport;
use App\Exports\ParticipantsPassedExport;
use App\Exports\ParticipantsRejectExport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use GuzzleHttp\Client;

class DonoreventController extends Controller
{
    public function index(Request $request){
        $page = 'event';
        if($request->has('search')){
            $donorEvents = Donorevent::where('name', 'LIKE', '%'.$request->search.'%')
                ->orWhere('date', 'LIKE', '%'.$request->search.'%')
                ->orWhere('location', 'LIKE', '%'.$request->search.'%')
                ->get();
        } else {
            $donorEvents = Donorevent::all();
        }
        return view('events.index', ['page' => $page, 'donor_events' => $donorEvents]);
    }

    public function eventCreate(Request $request){
        $this->validate($request, [
            'name' => 'required|min:5',
            'date' => 'required',
            'location' => 'required'
        ], [
            'required' => 'Wajib diisi'
        ]);

        $user = auth()->user()->id; //admin user have donor profile
        //handle if admin user haven't donor profile. will fill donor_id to ""
        $donor = "";
        (auth()->user()->donor == null) ? $donor = null : $donor = auth()->user()->donor->id;

        $donorevents = new Donorevent;
        $donorevents->name = $request->name;
        $donorevents->date = $request->date;
        $donorevents->location = $request->location;
        $donorevents->user_id = $user;
        $donorevents->donor_id = $donor;
        $donorevents->save();

        return redirect('/donorevent')->with('success', 'Berhasil membuat kegiatan baru');
    }

    public function edit($id){
        $page = 'event';
        $donorevent = Donorevent::find($id);
        return view('events.edit', ['page' => $page, 'donor_event' => $donorevent]);
    }

    public function eventEdit(Request $request, $id){
        $this->validate($request, [
            'name' => 'required|min:5',
            'date' => 'required',
            'location' => 'required'
        ], [
            'required' => 'Wajib diisi'
        ]);

        $donorEvent = Donorevent::find($id);
        $donorEvent->update($request->all());

        return redirect('/donorevent')->with('success', 'Berhasil mengupdate kegiatan '.$donorEvent->name);
    }

    public function participantLoloskan(Request $request, $event_id, $donor_id){
        $donor = Donor::find($donor_id);
        //$request->pk mengarah ke data-pk="{{$event->id}}"
        $donor->donorevent()->updateExistingPivot($event_id, ['status' => 'passed']);

        $name = '';
        if(getAge($donor->id) > 40 ){
            if($donor->gender == 'l'){
                $name = 'Bapak ' .$donor->fullname;
            }elseif ($donor->gender == 'p') {
                $name = 'Ibu ' .$donor->fullname;
            }
        } else {
            if($donor->gender == 'l'){
                $name = 'Mas ' .$donor->fullname;
            }elseif ($donor->gender == 'p') {
                $name = 'Mbak ' .$donor->fullname;
            }
        }
        // sending WA
        $client = new Client([
            'base_uri' => 'http://apiwabroadcast.herokuapp.com',
            'timeout'  => 5.0,
        ]);
        $client->request('POST', '/send-message/085647701900', [
            'http_errors' => false,
            'form_params' => [
                'receiver' => $donor->phone,
                'text' => 'Terimakasih *' .$name. '* yang telah melakukan donor darah bersama PRPM Walen. Semoga Allah membalas kebaikan ' .$name. ' dengan yang lebih baik.'
            ]
        ]);
        usleep(mt_rand(1000000,3000000));
        $number_format = substr($donor->phone, 0, 1) == '0' ? '@62' .substr($donor->phone, 1). '@c.us' : '@' .$donor->phone. '@c.us';

        $client->request('POST', '/send-group-message/085647701900', [
            'http_errors' => false,
            'form_params' => [
                'groupId' => '6287836420202-1438088945@g.us',
                'text' => 'Terimakasih *' .$name. '* yang telah melakukan donor darah bersama PRPM Walen. Semoga Allah membalas kebaikan ' .$name. ' dengan yang lebih baik.'
            ]
        ]);

        return redirect()->back()->with('success', 'Berhasil meloloskan '.$donor->fullname);
    }

    public function participantTolak(Request $request, $event_id, $donor_id){
        $donor = Donor::find($donor_id);
        //$request->pk mengarah ke data-pk="{{$event->id}}"
        $donor->donorevent()->updateExistingPivot($event_id, ['status' => 'reject']);

        $name = '';
        if(getAge($donor->id) > 40 ){
            if($donor->gender == 'l'){
                $name = 'Bapak ' .$donor->fullname;
            }elseif ($donor->gender == 'p') {
                $name = 'Ibu ' .$donor->fullname;
            }
        } else {
            if($donor->gender == 'l'){
                $name = 'Mas ' .$donor->fullname;
            }elseif ($donor->gender == 'p') {
                $name = 'Mbak ' .$donor->fullname;
            }
        }

        // sending WA
        $client = new Client([
            'base_uri' => 'http://apiwabroadcast.herokuapp.com',
            'timeout'  => 5.0,
        ]);
        $client->request('POST', '/send-message/085647701900', [
            'http_errors' => false,
            'form_params' => [
                'receiver' => $donor->phone,
                'text' => 'Terimakasih ' .$name. ' yang telah berpartisipasi dalam kegiatan donor darah bersama PRPM Walen.'
            ]
        ]);
        usleep(mt_rand(1000000,3000000));
        $number_format = substr($donor->phone, 0, 1) == '0' ? '@62' .substr($donor->phone, 1). '@c.us' : '@' .$donor->phone. '@c.us';

        $client->request('POST', '/send-group-message/085647701900', [
            'http_errors' => false,
            'form_params' => [
                'groupId' => '6287836420202-1438088945@g.us',
                'text' => 'Terimakasih *' .$name. '* yang telah berpartisipasi dalam kegiatan donor darah bersama PRPM Walen.'
            ]
        ]);

        return redirect()->back()->with('success', 'Berhasil menolak '.$donor->fullname);
    }

    public function eventDelete($id){
        $donorEvent = Donorevent::find($id);

        $donorEvent->delete($donorEvent);
        return redirect('/donorevent')->with('success', 'Berhasil menghapus kegiatan '.$donorEvent->name);
    }

    //show participant
    public function participant($id){
        $page = 'event';
        $donorEvent = Donorevent::find($id); //get pivot from frontend

        return view('events.participant', ['page' => $page, 'data_event' => $donorEvent]);
    }

    //show processing participant
    public function participantProcessing($event_id){
        $page = 'event';
        //get pivot from backend. usually from frontend
        $donorEvent = Donorevent::find($event_id)->donor()->where('status', 'processing')->get();

        //get event name and date
        $nameDate = Donorevent::find($event_id);

        return view('events.participantProcess', ['page' => $page, 'data_event' => $donorEvent, 'name_date' => $nameDate]);
    }

    //show passed participant
    public function participantPassed($event_id){
        $page = 'event';
        //get pivot from backend. usually from frontend
        $donorEvent = Donorevent::find($event_id)->donor()->where('status', 'passed')->get();
        // $donorEvent->donor()->where('status', 'passed')->get(); //can't use this. must be combined with query find

        //get event name and date
        $nameDate = Donorevent::find($event_id);

        return view('events.participantPassed', ['page' => $page, 'data_event' => $donorEvent, 'name_date' => $nameDate]);
    }

    //show reject participant
    public function participantReject($event_id){
        $page = 'event';
        //get pivot from backend. usually from frontend
        $donorEvent = Donorevent::find($event_id)->donor()->where('status', 'reject')->get();

        //get event name, date & id
        $nameDate = Donorevent::find($event_id);

        return view('events.participantReject', ['page' => $page, 'data_event' => $donorEvent, 'name_date' => $nameDate]);
    }

    //export participant in excel format
    public function exportParticipants($event_id) 
    {
        $fileName = 'daftarPesertaDonor' . Carbon::now()->setTimezone('Asia/Jakarta')->format('d-m-Y H:i') . '.xlsx';
        // ParticipantsExport me-return data dari table donors yg ikut kegiatan
        return Excel::download(new ParticipantsExport($event_id), $fileName);
    }

    //export participant passed in excel format
    public function exportParticipantsPassed($event_id) 
    {
        $fileName = 'pesertaDonorLolos' . Carbon::now()->setTimezone('Asia/Jakarta')->format('d-m-Y H:i') . '.xlsx';
        // ParticipantsExport me-return data dari table donors yg lolos
        return Excel::download(new ParticipantsPassedExport($event_id), $fileName);
    }

    //export participant reject in excel format
    public function exportParticipantsReject($event_id) 
    {
        $fileName = 'pesertaDonorTidakLolos' . Carbon::now()->setTimezone('Asia/Jakarta')->format('d-m-Y H:i') . '.xlsx';
        // ParticipantsExport me-return data dari table donors yg lolos
        return Excel::download(new ParticipantsRejectExport($event_id), $fileName);
    }
}
