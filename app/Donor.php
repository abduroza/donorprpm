<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donor extends Model
{
    protected $fillable = ['fullname','nik', 'gender', 'birth_place', 'birth_date', 'phone', 'religion', 'blood_group', 'address', 'avatar', 'user_id'];


    //relation
    public function user (){
        return $this->belongsTo(User::class);
    }

    public function donorevent(){
        return $this->belongsToMany(Donorevent::class)->withPivot(['status'])->withTimestamps();
    }

    //helpers
    //function to check avatar
    public function getAvatar(){
        if(!$this->avatar){
            return asset('images/avatarDefault.png');
        }
        return asset('images/'.$this->avatar);
    }

    //function to count passed for a donors
    function donorEventTotalPassed(){
        return $this->donorevent()->where('status', 'passed')->count();
    }

    //used in profile
    function donorEventPassedLast(){
        if ($this->donorevent()->where('status', 'passed')->exists()){
            //date('d-m-Y', strtotime)) used to convert format from 0000-00-00 to 00-00-0000
            return date('d-m-Y', strtotime($this->donorevent()->where('status', 'passed')->get()->last()->date));
        } else {
            return "00-00-0000";
        }
    }
    
}
