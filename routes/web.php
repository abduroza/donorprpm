<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@home')->name('home');
Route::get('/about', 'SiteController@about');
Route::get('/gallery', 'SiteController@gallery');
Route::get('/contact', 'SiteController@contact');

//route user register
Route::get('/registeeerrr', 'UserController@register');
Route::post('/userregister', 'UserController@create');

//route user login
Route::get('/login', 'UserController@login')->name('login');
Route::post('/userlogin', 'UserController@userLogin');

//rute user logout
Route::get('/logout', 'UserController@logout');

//route only access by authorization user. This route group instead of ->middleware('auth');
Route::group(['middleware' => ['auth', 'checkRole:admin']], function(){ //the key of checkRole must be same in Kernel.php
    //user route
    Route::get('/user', 'UserController@index');
    Route::get('/user/{id}/edit', 'UserController@edit');
    Route::post('/user/{id}/useredit', 'UserController@userEdit');
    Route::get('/user/{user_id}/userdelete', 'UserController@userDelete');

    //donor route
    Route::get('/donor', 'DonorController@index');
    Route::get('/donor/create', 'DonorController@create');
    Route::post('/donor/donorcreate', 'DonorController@donorCreate');
    Route::get('/donor/{id}/profile', 'DonorController@profile');
    Route::get('/donor/{id}/edit', 'DonorController@edit');
    Route::post('/donor/{id}/donoredit', 'DonorController@donorEdit');
    Route::get('/donor/{donor_id}/donordelete', 'DonorController@donorDelete');
    Route::post('/donor/{donor_id}/addevent', 'DonorController@addEvent');
    Route::get('/donor/{donor_id}/{donorevent_id}/deleteevent', 'DonorController@deleteEvent');
    Route::get('/donor/broadcast', 'DonorController@broadcastView');
    Route::post('/donor/broadcast', 'DonorController@broadcast');

    //donor event route
    Route::get('/donorevent', 'DonoreventController@index'); //make new event using modal. So no need new route, because using route index
    Route::post('/donorevent/eventcreate', 'DonoreventController@eventCreate');
    Route::get('/donorevent/{id}/edit', 'DonoreventController@edit');
    Route::post('/donorevent/{id}/eventedit', 'DonoreventController@eventEdit');
    Route::get('/donorevent/{id}/eventdelete', 'DonoreventController@eventDelete');
    Route::get('/donorevent/{id}/participant', 'DonoreventController@participant');
    Route::get('/donorevent/{event_id}/participantprocessing', 'DonoreventController@participantProcessing');
    Route::get('/donorevent/{event_id}/participantpassed', 'DonoreventController@participantPassed');
    Route::get('/donorevent/{event_id}/participantreject', 'DonoreventController@participantReject');
    Route::get('/donorevent/{event_id}/{donor_id}/participantloloskan', 'DonoreventController@participantLoloskan');
    Route::get('/donorevent/{event_id}/{donor_id}/participantTolak', 'DonoreventController@participantTolak');

    //dashboard route
    Route::get('/dashboard/a', 'DashboardController@goldarA');
    Route::get('/dashboard/b', 'DashboardController@goldarB');
    Route::get('/dashboard/ab', 'DashboardController@goldarAB');
    Route::get('/dashboard/o', 'DashboardController@goldarO');

    //export excel
    Route::get('/donor/export', 'DonorController@export');
    Route::get('/donorevent/{event_id}/excel', 'DonoreventController@exportParticipants');
    Route::get('/donorevent/passed/{event_id}/excel', 'DonoreventController@exportParticipantsPassed');
    Route::get('/donorevent/reject/{event_id}/excel', 'DonoreventController@exportParticipantsReject');
});

// Route::get('/dashboard', 'DashboardController@index');
Route::group(['middleware' => ['auth', 'checkRole:admin,donor,guest']], function(){
    Route::get('/dashboard', 'DashboardController@index');
});


