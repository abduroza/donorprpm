<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route bawaan dari laravel untuk menghandle auth
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//route for jquery ajax edit status
Route::post('/donor/{donor_id}/editstatus', 'ApiController@editStatus');
